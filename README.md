# Guet_Beamer_Theme
本模板为桂林电子科技大学Beamer模板
[![](https://img.shields.io/badge/license-LPPL-blue)](https://www.latex-project.org/lppl/) [![](https://img.shields.io/github/last-commit/YanMing-lxb/Guet_Beamer_Theme)](https://github.com/YanMing-lxb/Guet_Beamer_Theme/zipball/master) [![](https://img.shields.io/github/issues/YanMing-lxb/Guet_Beamer_Theme)](https://github.com/YanMing-lxb/Guet_Beamer_Theme/issues)

该模板基于 [UESTC-Beamer](https://www.overleaf.com/latex/templates/uestc-beamer-theme/ybqzdsgvrfdq).

欢迎老师同学们使用该模板。

## 写作工具


- 画表神器 https://www.tablesgenerator.com/
- 写公式神器 https://mathpix.com/
- 文献 bib 整理神器 https://dblp.uni-trier.de/
- Latex 画图画表常用命令 https://en.wikibooks.org/wiki/LaTeX/Floats,_Figures_and_Captions#Tip
